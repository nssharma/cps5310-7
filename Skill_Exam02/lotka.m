function yp = lotka(t,y)
yp(1) = (1 - y(2))*y(1)  %y1=x,y2=y, yp1= dx/dt

yp(2) = (-1 + y(1))*y(2) - y(2)*y(3) %y(3) = z

yp(3) = (-1+y(2))*y(3)

yp = diag([1 - y(2), -1 + y(1)-y(3), -1+y(2)])*y;
