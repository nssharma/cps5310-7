%SIRFIT1 :
S= [762 740 650 400 250 120 80 50 20 18 15 13 10 ];
I= [1 20 80 220 300 260 240 190 120 80 20 5 2 ];
days= [0 3 4 5 6 7 8 9 10 11 12 13 14 ];
for  k= 1:10
Y(k) = (1/I(k+2))*(I(k+3) - I(k+1))/2 ;
X(k) = S(k+2) ;
end
plot ( X,Y,'o')

%The plot created by this code is given as Fig1. Fitting a line to this data , we ?nd
%a = .0036
%b = .9395 