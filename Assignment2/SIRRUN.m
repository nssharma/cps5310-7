%SIRRUN:DIRECT
guess = [0.0026; 0.5058 ];
[p,error]=fminsearch(@sirerr,guess)
[t,y]= ode23(@sirode,[0,14],[762;1],[],p) ;
days= [0 3 4 5 6 7 8 9 10 11 12 13 14];
S = [762 740 650 400 250 120 80 50 20 18 15 13 10 ];
I = [1 20 80 220 300 260 240 190 120 80 20 5 2 ];
subplot ( 2 ,1 ,1 )
plot (t ,y(:,1) ,days,S,'o')
subplot(2,1,2 )
plot(t,y(:,2 ),days,I,'o')
%Final error calculation
[t ,y] = ode23( @sirode,days,[S(1);I(1)],[],p) ;
error = sqrt(error)